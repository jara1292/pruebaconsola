﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string nombre = "José";
            string app = "Rodriguez";
            string apm = "Arzate";
            DateTime fchn =  Convert.ToDateTime("1992-11-12");

            Alumno alumno = new Alumno(nombre,app,apm,fchn);

            var datos = alumno.ObtenerNombre();

            Console.WriteLine(datos);
            Console.ReadKey();

        }
    }
}

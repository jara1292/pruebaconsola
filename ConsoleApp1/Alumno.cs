﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Alumno
    {
        public Alumno(string _Nombre,string _Ap_Paterno,string _Ape_Materno)
        {
            this.Nombre = _Nombre;
            this.Ap_Paterno = _Ap_Paterno;
            this.Ap_Paterno = _Ape_Materno;
        }

        public Alumno(string _Nombre, string _Ap_Paterno, string _Ape_Materno,DateTime _Fecha_Nacimiento)
        {
            this.Nombre = _Nombre;
            this.Ap_Paterno = _Ap_Paterno;
            this.Ap_Paterno = _Ape_Materno;
            this.Fecha_Nacimiento = _Fecha_Nacimiento;
        }

        public string Nombre { get; set; }
        public string Ap_Paterno { get; set; }
        public string Ap_Materno { get; set; }
        public DateTime Fecha_Nacimiento { get; set; }
        public bool Estatus { get; set; }


        public string ObtenerNombre()
        {
            return $"{Nombre} - {Ap_Paterno} - {Ap_Materno} -- {Fecha_Nacimiento}";

        }
    }

}
